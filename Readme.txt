Changes:

Build the project with -DSQLITE_MAX_LENGTH=2147483647 (using 2 GB in Data base)
Improve CppSQLite3Statement (CppSQLite3.h):
    In class CppSQLite3Statement added new method "bindDirect", which write memory direct to Data base (not to memory and then to Data base).
    More deatil see http://www.sqlite.org/c3ref/bind_blob.html, http://stackoverflow.com/questions/16043734/sqlite3-bind-text-sqlite-static-vs-sqlite-transient-for-c-string